package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import um.fds.agl.ter22.entities.Group;
import um.fds.agl.ter22.entities.Sujet;

public interface GroupRepository<G extends Group> extends CrudRepository<G, Long>{
    Sujet save(@Param("terProject") Sujet terProject);
}