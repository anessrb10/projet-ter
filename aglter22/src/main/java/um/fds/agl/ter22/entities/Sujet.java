package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Sujet {
    private String titre;
    private @ManyToOne Teacher LastNameProf;
    private @ManyToOne Teacher teacher2;
    private @Id @GeneratedValue Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sujet(String titre, Teacher LastNameProf,Teacher teacher2, Long id) {
        this.titre = titre;
        this.LastNameProf = LastNameProf;
        this.id = id;
        this.teacher2=teacher2;
    }

    public Sujet(String titre, Teacher LastNameProf){
        this.titre=titre;
        this.LastNameProf=LastNameProf;

    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Teacher getLastNameProf() {
        return LastNameProf;
    }

    public void setLastNameProf(Teacher LastNameProf) {
        this.LastNameProf = LastNameProf;
    }

    public Teacher getTeacher2() {
        return teacher2;
    }

    public Sujet() {
    }


}