package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.SujetForm;
import um.fds.agl.ter22.forms.TeacherForm;
import um.fds.agl.ter22.services.TERManagerService;
import um.fds.agl.ter22.services.SujetService;

public class SujetController implements ErrorController {

    @Autowired
    private SujetService sujetService;
    @Autowired
    private TERManagerService terManagerService;


    @GetMapping("/listSujets")
    public Iterable<Sujet> getSujets(Model model) {
        Iterable<Sujet> sujets = sujetService.getSujets();
        model.addAttribute("sujets", sujets);
        return sujets;
    }

    @GetMapping(value = {"/addSujet"})
    public String showAddSujetPage(Model model) {
        SujetForm sujetForm = new SujetForm();
        model.addAttribute("sujetForm", sujetForm);

        return "addSujet";
    }

    @PostMapping(value = { "/addSujet"})
    public String addSujet(Model model, @ModelAttribute("SujetForm") SujetForm sujetForm) {
        Sujet s;
        if(sujetService.findById(sujetForm.getId()).isPresent()){
            // teacher already existing : update
            s = sujetService.findById(sujetForm.getId()).get();
            s.setTitre(sujetForm.getTitre());
        } else {
            // teacher not existing : create
            s=new Sujet(sujetForm.getTitre(), sujetForm.getLastNameProf());
        }
        SujetService.saveSujet(s);
        return "redirect:/listSujets";

    }

    @GetMapping(value = {"/showSujetUpdateForm/{id}"})
    public String showSujetUpdateForm(Model model, @PathVariable(value = "id") long id){

        SujetForm sujetForm = new SujetForm(id,
                sujetService.findById(id).get().getTitre(),
                sujetService.findById(id).get().getLastNameProf(),
                sujetService.findById(id).get().getTeacher2());
        model.addAttribute("sujetForm", sujetForm);
        return "updateSujet";
    }
}
