package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Teacher;

public class SujetForm {

    private long id;
    private String titre;
    private Teacher lastNameProf;
    private Teacher teacher2;


    public SujetForm(long id, String titre, Teacher lastNameProf, Teacher teacher2) {
        this.titre= titre;
        this.lastNameProf = lastNameProf;
        this.id=id;
        this.teacher2 = teacher2;
    }

    public SujetForm() {}
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Teacher getLastNameProf() {
        return lastNameProf;
    }

    public void setLastNameProf(Teacher lastNameProf) {
        this.lastNameProf = lastNameProf;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public void setTeacher2(Teacher teacher2) {
        this.teacher2 = teacher2;
    }

    public Teacher getTeacher2() {
        return this.teacher2;
    }



}