

package um.fds.agl.ter22.repositories;

        import org.springframework.data.repository.CrudRepository;
        import um.fds.agl.ter22.entities.Sujet;

public interface SujetRepository extends CrudRepository<Sujet, Long> {
}